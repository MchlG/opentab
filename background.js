//background.js


// Called when the user clicks on the browser action.
// chrome.browserAction.onClicked.addListener(function(tab) {
//   // Send a message to the active tab
//   chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
//     var activeTab = tabs[0];
//     console.log("active tab" + activeTab);

//     chrome.tabs.sendMessage(activeTab.id, {"message": "clicked_browser_action"});
//     chrome.tabs.sendMessage(activeTab.id, {"message": "TEST"});

//   });
// });



// remove "default_popup": "popup.html" from manijfest.json to have this triggered whenever extension icpm is clicked
// chrome.browserAction.onClicked.addListener(function(tab) {

// 	console.log("browser action clicked");
	// chrome.windows.getAll({populate:true},function(windows){

	// 	allOpenTabs = []; //clear tabs array

	//   windows.forEach(function(window){
	// 		console.log("window coming up...");
	// 		console.log(window);
	// 	window.tabs.forEach(function(tab){
	// 	  //collect all of the urls here, I will just log them instead
	// 	  console.log(tab.url);
	// 	  allOpenTabs.push(tab.url); //push tab into tabs array
	// 	  // chrome.tabs.sendMessage(tab.id, {tab.id: "clicked_browser_action2"});
	// 	});
	//   });
	  
	//   for (i = 0; i < allOpenTabs.length; i++){
	//   	console.log("OpenTab #" + i + allOpenTabs[i]);
	//   }

	// });
// });



chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    // console.log(sender.tab ?
    //             "from a content script:" + sender.tab.url :
    //             "from the extension");

	console.log("recieved message in background.js")
    
    if (request.action === "saveTabs") {
    	getAllOpenTabs()
	    sendResponse({farewell: "...saved tabs success"});
	} else if (request.action === "loadTabs"){
		loadAllSavedTabs()
	}

  });




function getAllOpenTabs(){

	console.log("saving tabs...")

	var windowsAndTabs = {}; 

	chrome.windows.getAll({populate:true},function(windows){

	  var windowIds =[];

	  windows.forEach(function(window){

	  	var allTabsInWindow = []; 
			// console.log(window);
		var id = window.id;
		var idString = id.toString();
		windowIds.push(idString);

		window.tabs.forEach(function(tab){
		  //collect all of the urls here
		  // console.log(tab.url);
		  // allOpenTabs.push(tab.url); //push tab into tabs array
		  allTabsInWindow.push(tab.url);
		  // chrome.tabs.sendMessage(tab.id, {tab.id: "clicked_browser_action2"});
		});

		windowsAndTabs[idString] = allTabsInWindow;

	  });
	  
	  // for (i = 0; i < allOpenTabs.length; i++){
	  // 	console.log("OpenTab # " + i + allOpenTabs[i]);
	  // }

	  saveWindowAndTabSettings(windowsAndTabs, windowIds);

	});

}

function saveWindowAndTabSettings(settingsObject, objectPropertyKeys){

	console.log("called save all open windows with " + settingsObject + "with properties " + objectPropertyKeys);
	
	chrome.storage.sync.clear(function(){
		// console.log("DID CLEAR SYNC ")
	
		chrome.storage.sync.set({"WindowAndTabSettings": settingsObject}, function(){
			 console.log("SETTINGS WAS SAVED : " + settingsObject);

			 didSaveTabSettings();
		});

		chrome.storage.sync.set({"propertyKeys": objectPropertyKeys}, function(){
			 console.log("saved properyy keys = " + objectPropertyKeys);
		});

	})
}

function didSaveTabSettings(){
	console.log("CALLED DID SAVE TAB SETTINGS");
	chrome.runtime.sendMessage({message: "tabs saved"}, function(response) {
		// console.log(response.farewell);
    });
};


function loadAllSavedTabs() {
	// console.log("loading all saved tabs...")

	chrome.storage.sync.get("propertyKeys", function(data){
		// console.log("got the property keys -> " + data["propertyKeys"])
		var propertyKeys = data["propertyKeys"];

		chrome.storage.sync.get("WindowAndTabSettings", function(data){
			// console.log("retrieved storage : " + data["WindowAndTabSettings"]);
			var settings = data["WindowAndTabSettings"]

			for (i = 0; i < propertyKeys.length; i++){
				// console.log(settings[propertyKeys[i]]);
				openWindowWith(settings[propertyKeys[i]]);
			}
		});

	});
}

function openWindowWith(tabs){

	checkForOpenTabs(function(tabsAlreadyOpen){
		// console.log("got tabs already open "+ tabsAlreadyOpen);

		var tabsNeedOpening = [];

		for (i = 0; i < tabs.length; i++){
			if (tabsAlreadyOpen.indexOf(tabs[i]) === -1){
				// console.log("tab not already open")
				tabsNeedOpening.push(tabs[i]);
			} 
		}

		if (tabsNeedOpening.length > 0) {
			chrome.windows.create({url: tabsNeedOpening}, function(window){
				console.log("Success, created window " + window.tabs);
			});
		}

	});

	
}

function checkForOpenTabs(callback){

	chrome.windows.getAll({populate:true},function(windows){

	  var tabsAlreadyOpen = []; 

	  windows.forEach(function(window){

		window.tabs.forEach(function(tab){
		  tabsAlreadyOpen.push(tab.url);
		});

	  });
	  	callback(tabsAlreadyOpen);
	});
}








////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

// var allOpenTabs = [];


// chrome.runtime.onMessage.addListener(
//   function(request, sender, sendResponse) {
//     // console.log(sender.tab ?
//     //             "from a content script:" + sender.tab.url :
//     //             "from the extension");

// 	console.log("recieved message in background.js")
    
//     if (request.action === "saveTabs") {
//     	getAllOpenTabs()
// 	    sendResponse({farewell: "...saved tabs success"});
// 	} else if (request.action === "loadTabs"){
// 		loadAllSavedTabs()
// 	}

//   });


// function getAllOpenTabs(){

// 	console.log("saving tabs...")

// 	chrome.windows.getAll({populate:true},function(windows){

// 	  allOpenTabs = []; //clear tabs array

// 	  windows.forEach(function(window){
// 			console.log(window);
		

// 		window.tabs.forEach(function(tab){
// 		  //collect all of the urls here, I will just log them instead
// 		  // console.log(tab.url);
// 		  allOpenTabs.push(tab.url); //push tab into tabs array
// 		  // chrome.tabs.sendMessage(tab.id, {tab.id: "clicked_browser_action2"});
// 		});
// 	  });
	  
// 	  // for (i = 0; i < allOpenTabs.length; i++){
// 	  // 	console.log("OpenTab # " + i + allOpenTabs[i]);
// 	  // }

// 	  saveAllOpenTabs(allOpenTabs);

// 	});

// }

// function saveAllOpenTabs(tabs){

// 	console.log("called save all open tabs with " + tabs);
// 	var totalTabs = tabs.length
// 	chrome.storage.sync.clear(function(){
// 		console.log("DID CLEAR SYNC ")
	
// 		chrome.storage.sync.set({"lastOpenTabs": tabs}, function(){
// 			console.log("TABS WAS SAVED : " + tabs);
// 		});

// 	})
// }

// function loadAllSavedTabs() {
// 	console.log("loading all saved tabs...")

// 	chrome.storage.sync.get("lastOpenTabs", function(data){
// 			console.log("retirved storage : " + data["lastOpenTabs"]);
// 			var tabs = data["lastOpenTabs"]

// 			if (tabs != null) {
// 				for (i = 0; i < tabs.length; i++){
// 					console.log(tabs[i]);
// 				}
// 			}

// 	});

// 	//load all the saved tabs into the window
// }


////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////











// chrome.runtime.onMessage.addListener(
//   function(request, sender, sendResponse) {
//     if( request.message === "clicked_save_button" ) {
      	
// 		console.log("save button action message recieved");
		// chrome.windows.getAll({populate:true},function(windows){

		//   allOpenTabs = []; //clear tabs array

		//   windows.forEach(function(window){
		// 		console.log("window coming up...");
		// 		console.log(window);
		// 	window.tabs.forEach(function(tab){
		// 	  //collect all of the urls here, I will just log them instead
		// 	  console.log(tab.url);
		// 	  allOpenTabs.push(tab.url); //push tab into tabs array
		// 	  // chrome.tabs.sendMessage(tab.id, {tab.id: "clicked_browser_action2"});
		// 	});
		//   });
		  
		//   for (i = 0; i < allOpenTabs.length; i++){
		//   	console.log("OpenTab #" + i + allOpenTabs[i]);
		//   }

		// });

//     } 
//   }
// );




// 
// chrome.tabs.query({},function(tabs){     
//     console.log("\n/////////////////////\n");
//     tabs.forEach(function(tab){
//       console.log(tab.url);
//     });
//  });
